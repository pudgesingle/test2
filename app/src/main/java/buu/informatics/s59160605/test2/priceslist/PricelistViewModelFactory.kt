package buu.informatics.s59160605.test2.priceslist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.IllegalArgumentException

class PricelistViewModelFactory(): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PricelistViewModel::class.java)) {
            return PricelistViewModel() as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}