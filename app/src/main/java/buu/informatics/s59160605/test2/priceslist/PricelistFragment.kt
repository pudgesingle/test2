package buu.informatics.s59160605.test2.priceslist


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import buu.informatics.s59160605.test2.R
import buu.informatics.s59160605.test2.databinding.FragmentPricelistBinding

/**
 * A simple [Fragment] subclass.
 */
class PricelistFragment : Fragment() {
    private lateinit var binding:FragmentPricelistBinding
    private  lateinit var viewModel: PricelistViewModel
    private lateinit var viewModelFactory: PricelistViewModelFactory
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_pricelist, container, false)
        binding.backbutton1.setOnClickListener{
            it.findNavController().navigate(PricelistFragmentDirections.actionPriceslistFragmentToHomeFragment())
        }
        viewModelFactory = PricelistViewModelFactory()
        Log.i("PricelistFragment", "Called ViewModelProviders.of")
        viewModel = ViewModelProviders.of(this,viewModelFactory).get(PricelistViewModel::class.java)
        return binding.root
    }


}
