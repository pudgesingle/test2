package buu.informatics.s59160605.test2.priceslist

import android.util.Log
import androidx.lifecycle.ViewModel

class PricelistViewModel : ViewModel(){
    init {
        Log.i("PricelistViewModel", "PricelistViewModel create!")
    }
    override fun onCleared() {
        super.onCleared()
        Log.i("PricelistViewModel", "PricelistViewModel destroyed!")
    }
}