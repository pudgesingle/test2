package buu.informatics.s59160605.test2.addlist

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import buu.informatics.s59160605.test2.database.DatabaseDao
import java.lang.IllegalArgumentException

class AddlistViewModelFactory(
    private var dataSource: DatabaseDao,
    private var application: Application
):ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AddlistViewModel::class.java)){
            return AddlistViewModel(dataSource,application) as T
        }
        throw IllegalArgumentException("Unknow ViewModel class")
    }
}