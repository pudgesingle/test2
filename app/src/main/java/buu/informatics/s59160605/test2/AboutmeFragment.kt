package buu.informatics.s59160605.test2


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import buu.informatics.s59160605.test2.databinding.FragmentAboutmeBinding

/**
 * A simple [Fragment] subclass.
 */
class AboutmeFragment : Fragment() {
    private lateinit var binding:FragmentAboutmeBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_aboutme, container, false)
        binding.backbutton.setOnClickListener {
            it.findNavController().navigate(R.id.action_aboutmeFragment_to_homeFragment)
        }
        return binding.root
    }


}
