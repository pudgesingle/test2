package buu.informatics.s59160605.test2.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "detail_table")
data class Kazed(
    @PrimaryKey(autoGenerate = true)
    var kazedId: Long = 0L,

    @ColumnInfo(name = "product_name")
    var name:String?,

    @ColumnInfo(name = "product_price")
    var price:Int
)
