package buu.informatics.s59160605.test2.addlist

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import buu.informatics.s59160605.test2.database.DatabaseDao
import buu.informatics.s59160605.test2.database.Kazed
import kotlinx.coroutines.*

class AddlistViewModel(
    val database: DatabaseDao,
    application: Application
) : AndroidViewModel(application) {
    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    var inputName = MutableLiveData<String?>()
    var inputPrice = MutableLiveData<String?>()

    private val _showAlert = MutableLiveData<Boolean>()
    val showAlert: LiveData<Boolean>
    get() = _showAlert

    init {
        Log.i("AddlistViewModel", "AddlistViewModel created!")
    }

    fun clickAdd(){
        uiScope.launch {
            if(checkInputNull()){
                Log.i("AddlistViewModel", "input have null")
                _showAlert.value = false
            }
            else{
                var newKazed = Kazed(name = inputName.value, price = Integer.parseInt(inputPrice.value!!))
                insert(newKazed)
                _showAlert.value = true
            }

        }
    }
    private fun checkInputNull() = (inputName.value == null || inputPrice.value == null)

    private suspend fun insert(kazed: Kazed){
        withContext(Dispatchers.IO){
            database.insert(kazed)
        }
    }

    override fun onCleared() {
        super.onCleared()
        Log.i("AddlistViewModel", "AddlistViewModel destroyed!")
        viewModelJob.cancel()
    }


}