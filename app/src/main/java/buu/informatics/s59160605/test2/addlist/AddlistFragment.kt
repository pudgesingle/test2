package buu.informatics.s59160605.test2.addlist


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import buu.informatics.s59160605.test2.R
import buu.informatics.s59160605.test2.database.KazedDB
import buu.informatics.s59160605.test2.databinding.FragmentAddlistBinding
import com.google.android.material.snackbar.Snackbar

/**
 * A simple [Fragment] subclass.
 */
class AddlistFragment : Fragment() {
    private lateinit var binding:FragmentAddlistBinding
    private lateinit var viewModel: AddlistViewModel
    private lateinit var viewModelFactory: AddlistViewModelFactory
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_addlist, container, false)
        binding.addbutton2.setOnClickListener {
            it.findNavController().navigate(AddlistFragmentDirections.actionAddlistFragmentToHomeFragment())
        }
        binding.backbutton1.setOnClickListener {
            it.findNavController().navigate(AddlistFragmentDirections.actionAddlistFragmentToHomeFragment())
        }

        val application = requireNotNull(this.activity).application
        val dataSource = KazedDB.getInstance(application).databaseDao
        viewModelFactory = AddlistViewModelFactory(dataSource,application)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(AddlistViewModel::class.java)


        viewModel.showAlert.observe(this, Observer {
            if(it){
                Snackbar.make(
                    activity!!.findViewById(android.R.id.content),
                    "save success",
                    Snackbar.LENGTH_SHORT
                ).show()
            }else{
                Toast.makeText(context, "input have null", Toast.LENGTH_LONG).show()
            }

        })

        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        return binding.root
    }


}
