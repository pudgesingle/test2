package buu.informatics.s59160605.test2.result


import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import buu.informatics.s59160605.test2.R
import buu.informatics.s59160605.test2.database.KazedDB
import buu.informatics.s59160605.test2.databinding.FragmentResultBinding

/**
 * A simple [Fragment] subclass.
 */
class ResultFragment : Fragment() {
    private lateinit var binding:FragmentResultBinding
    private lateinit var viewModel: ResultViewModel
    private lateinit var viewModelFactory: ResultViewModelFactory
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_result, container, false)
        binding.backbutton1.setOnClickListener {
            it.findNavController().navigate(ResultFragmentDirections.actionResultFragmentToHomeFragment())
        }

        binding.sharebutton.setOnClickListener {
            startActivity(getShareIntent())
        }

        val application = requireNotNull(this.activity).application
        val dataSource = KazedDB.getInstance(application).databaseDao
        viewModelFactory = ResultViewModelFactory(dataSource,application)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ResultViewModel::class.java)

        val adapter = ResultAdapter()
        binding.kazedList.adapter = adapter

        viewModel.kazed.observe(viewLifecycleOwner, Observer {
            it?.let {
                adapter.data = it
            }
        })

        setHasOptionsMenu(true)
        return binding.root
    }

    private fun getShareIntent() : Intent {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.setType("text/plain")
            .putExtra(Intent.EXTRA_TEXT, getString(R.string.share_button))
        return shareIntent
    }


}
