package buu.informatics.s59160605.test2.home


import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import buu.informatics.s59160605.test2.R
import buu.informatics.s59160605.test2.databinding.FragmentHomeBinding

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment() {
    private lateinit var binding : FragmentHomeBinding
    private lateinit var viewModel: HomeViewModel
    private lateinit var viewModelFactory: HomeViewModelFactory
            override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_home, container, false)

        binding.addbutton.setOnClickListener {
            it.findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToAddlistFragment())
        }
        binding.checkpricesbutton.setOnClickListener {
            it.findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToPriceslistFragment())
        }
        binding.checkprofitbutton.setOnClickListener {
            it.findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToResultFragment())
        }
                viewModelFactory = HomeViewModelFactory()

                Log.i("HomeFragment", "Called ViewModelProviders.of")
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.menu, menu)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item!!,
            view!!.findNavController())
                || super.onOptionsItemSelected(item)
    }
}
