package buu.informatics.s59160605.test2.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.PrimaryKey
import androidx.room.Query

@Dao
interface DatabaseDao{
    @Insert
    fun insert(kazed: Kazed)

    @Query("SELECT * FROM detail_table ")
    fun getAll():LiveData<List<Kazed>>
}