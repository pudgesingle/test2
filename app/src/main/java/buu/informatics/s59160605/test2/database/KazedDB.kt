package buu.informatics.s59160605.test2.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(entities = [Kazed::class], version = 1,exportSchema = false)
abstract class KazedDB : RoomDatabase(){

    abstract val databaseDao: DatabaseDao

    companion object{

        @Volatile
        private  var INSTANCE: KazedDB? = null

        fun getInstance(context: Context): KazedDB{
            synchronized(this){
                var instance = INSTANCE

                if(instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        KazedDB::class.java,
                        "kazed_history_database"
                    )
                        .fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance
                }
                return  instance
            }
        }
    }
}