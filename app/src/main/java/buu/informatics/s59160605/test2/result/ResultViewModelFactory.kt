package buu.informatics.s59160605.test2.result

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import buu.informatics.s59160605.test2.database.DatabaseDao
import java.lang.IllegalArgumentException

class ResultViewModelFactory(
    private var dataSource: DatabaseDao,
    private var application: Application
): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ResultViewModel::class.java)){
            return ResultViewModel(dataSource,application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}