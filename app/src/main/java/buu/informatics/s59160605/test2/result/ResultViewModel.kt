package buu.informatics.s59160605.test2.result

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import buu.informatics.s59160605.test2.database.DatabaseDao
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job

class ResultViewModel(
    val database: DatabaseDao,
    application: Application
) : AndroidViewModel(application) {

    var kazed = database.getAll()

    init {
        Log.i("ResultViewModel","ResultViewModel created!")
    }
    override fun onCleared() {
        super.onCleared()
        Log.i("ResultViewModel", "ResultViewModel destroyed!")
    }
}